number_to_check = int(input("Enter a number\n"))

is_prime = True
for i in range(2, number_to_check):
    if number_to_check % i == 0:
        is_prime = False
        break

if is_prime:
    print(number_to_check, "is a prime number")
else:
    print(number_to_check, "is not a prime number")
